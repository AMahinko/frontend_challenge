/**
 * Request next set of members thought ApiGetClient
 */
var ApiGetClient = new Class({
  Extends: Request,
  options: {
    headers: {
      Accept: 'application/json'
    },
    method: 'get',
    queryparams: null,
    apibaseurl: '',
    apipath: ''
  },

  initialize: function(options) {
    this.parent(options);
    this.setApiUrl();
    this.setQueryParams(this.options.queryparams);
    this.send();
  },

  setApiUrl: function() {
    this.apiurl = this.options.apibaseurl;
    this.apiurl += this.options.apipath;
  },

  // refactored to remove redundant code shuffling hashes around, seeing as
  // params are passed as a hash anyway
  setQueryParams: function(params) {
    if (Object.getLength(params) > 0) {
      this.apiurl += '?' + Object.toQueryString(params);
      this.apiurl = this.apiurl.replace("'", '%27');
    }
  },

  //removed getApiUrl function as it was redundant
  //removed further redundancies in send function, to aid in maintence
  //leaving in if statement, as that would give the send function
  send: function(query_string) {
    if (typeof(query_string) === 'string') {
      this.setOptions({
        postdata: query_string.parseQueryString()
      });
    }
    this.setOptions({
      url: this.apiurl,
      data: this.options.postdata
    });
    this.parent(query_string);
  }

});
