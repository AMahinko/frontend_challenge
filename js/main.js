// Your code here ...


window.addEvent('domready', function() {

  //Populating first 5 clients
  ApiClientLoop(1, 1)

  // Button event handler
  $$('#js-btn-action-more').addEvent('click', function(){
    // Loop will iterate up to 5 pages (that's what I took 'page size is 5' to mean.), assuming we want the rest of the clients to populate all at once
    ApiClientLoop(2, 5);
    //Removes the button, so users can't spam the page with an infinte list of clients
    $$(this).setStyle('display', 'none');
  });

  //Main loop for API calls, to keep code as DRY as possible, as well as making it easy to add more client pages in the future.
  function ApiClientLoop(firstPage, lastPage) {

    for (pageCount = firstPage; pageCount <= lastPage; pageCount++) {

      var moreClients = new ApiGetClient(
        {
          //Strangely, the order of the pages is not consistant on the same seed / different requests, so every new request orders the client pages differently
          //I'm going to assume that the order's not vital for the site.
          queryparams:{'results':'5', 'page':pageCount, 'seed':'1234asdv'},
          apibaseurl:'https://randomuser.me',
          apipath:'/api',
          onSuccess: function(responseText){

            var results = JSON.parse(responseText)['results']

            for (count = 0; count <= results.length - 1; count++) {

              // defining relevant information as variables. Done with basic concactination to keep things readable and transparent.
              var name = results[count]['name']['first'].capitalize() + ' ' + results[count]['name']['last'].capitalize();
              var email = results[count]['email']
              // replaced 'example' with 'igloo' in the result. Partially for the sake of accuracy but mostly for kicks.
              email = email.replace('example', 'igloo')
              var imageurl = results[count]['picture']['large']

              // wrangling said info into a div for css to style
              $$('.clients').appendHTML(

                "<div class=\"client\">" +
                  "<img src=\"" + imageurl + "\" class=\"client-image\">" +
                  "<h4>" + name + "</h4>" +
                  "<p>" + email + "</p>" +
                "</div>"

              )

            }
          },
      });
    };
  };
});
