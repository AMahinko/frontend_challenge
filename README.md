# Code-Challenge-WEB

Here's my submission for the Code Challenge, functioning and responsive. It scales nicely in the responsive viewer, however the banner begins to break at about 700px wide if you just shrink your browser window. At that point I'd imagine the site would switch to a different, more mobile-friendly layout altogether.

All the code is optimized for readability, as opposed to resource efficiency. 

Looking forward to working with you,  
- Alex